package kosmokapusta.com.mulitaskservice;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

public class ParallelService extends Service {
    private final static int INTERVAL = 1000 * 120;
    private final static String LOG = "SERVICE";

    Timer timer;

    public ParallelService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Toast.makeText(this, getResources().getString(R.string.service_started), Toast.LENGTH_LONG).show();

        timer = new Timer();
        timer.schedule( new TimerTask() {
            public void run() {
                CallAsyncTask task1 = new CallAsyncTask() {
                    @Override
                    public void receiveData(String response) {
                        String res = getStringFromResponse(response, "tasks");
                        Log.d(LOG, "task 1: " + res);
                    }
                };

                CallAsyncTask task2 = new CallAsyncTask() {
                    @Override
                    public void receiveData(String response) {
                        String res = getStringFromResponse(response, "version");
                        Log.d(LOG, "task 2: " + res);
                    }
                };

                CallAsyncTask task3 = new CallAsyncTask() {
                    @Override
                    public void receiveData(String response) {
                        String res = getStringFromResponse(response, "author");
                        Log.d(LOG, "task 3: " + res);
                    }
                };

                task1.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[] { "http://mobapply.com/tests/json1" });
                task2.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[] { "http://mobapply.com/tests/json2" });
                task3.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[]{"http://mobapply.com/tests/json3"});
            }
        }, 0, INTERVAL);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(timer != null) {
            timer.cancel();
            timer = null;
        }
        Toast.makeText(this, getResources().getString(R.string.service_stopped), Toast.LENGTH_LONG).show();
    }

    private String getStringFromResponse(String response, String type) {
        JsonElement jelement = new JsonParser().parse(response);
        JsonObject jobject = jelement.getAsJsonObject();
        String result = "";

        switch (type) {
            case "tasks":
                JsonArray jarray = jobject.getAsJsonArray("tasks");
                JsonObject taskobj = jarray.get(1).getAsJsonObject();
                String id = taskobj.get("ID").toString();
                String title = taskobj.get("title").toString();
                String price = taskobj.getAsJsonArray("prices").get(0).getAsJsonObject().get("price").toString();
                result = id + " " + title + " " + price;
                break;
            case "author":
                result = jobject.get("author").toString();
                break;
            case "version":
                result = jobject.get("version").toString();
                break;
            default:
                result = "Invalid data";
                break;
        }

        return result;
    }
}
