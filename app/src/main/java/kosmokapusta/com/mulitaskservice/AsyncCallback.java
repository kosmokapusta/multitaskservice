package kosmokapusta.com.mulitaskservice;

public interface AsyncCallback {
    public void receiveData(String response);
}
